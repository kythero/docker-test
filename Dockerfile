FROM ubuntu

RUN apt-get update
RUN apt-get install -y apt-utils curl git
RUN apt-get install -y python3.5 
RUN apt-get install -y libapache2-mod-wsgi-py3
RUN apt-get install -y python3-pip
RUN pip3 install --upgrade pip

COPY . /my_app
WORKDIR /my_app

RUN pip install -r others/requirements.txt

CMD exec gunicorn my_app.wsgi:application --bind 0.0.0.0:8080 --workers 3

